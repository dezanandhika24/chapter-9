import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Home from './pages/home'
import Contact from './pages/contact'
import NotFound from './pages/notFound'

class App extends Component {
  
  render() { 
    return ( 
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/contact" component={Contact} />
          <Route component={NotFound} />
        </Switch>
      </Router>
    )
  }
}
 
export default App;