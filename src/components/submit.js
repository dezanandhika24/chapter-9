import React, { Component } from 'react';

class Submit extends Component {
    state = {
        nama : '',
        password: ''
    }

    handleOnChanges = e => {
        this.setState({
            nama: e.target.value
        })

        e.preventDefault()
    }

    handleOnSubmit = e => {
        alert('Nama kamu adalah : ' + this.state.nama)
        e.preventDefault()
    }

    render() { 
        return ( 
            <div>
                <form onSubmit={this.handleOnSubmit}>
                    <label>Nama :</label> <input type="text" value={this.state.nama} onChange={this.handleOnChanges} />
                    <br/>
                    <input type="submit"/>
                </form>


            </div>

         );
    }
}
 
export default Submit;